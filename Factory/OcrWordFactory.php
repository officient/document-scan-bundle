<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;

use Officient\DocumentScan\Entity\OcrObject;
use Officient\DocumentScan\Entity\OcrWord;
use Officient\DocumentScan\Entity\OcrBoundingBox;

class OcrWordFactory implements OcrWordFactoryInterface
{
    /**
     * @param DOMElement $node
     * @param int $pageNumber
     * @param OcrObject $parent
     * @return OcrWord
     */
    public function make(DOMElement $node, int $pageNumber, OcrObject $parent): OcrWord
    {
        list($absMinX, $abxMinY, $absMaxX, $absMaxY) = OcrBoundingBox::getBoxCoordinatesFromHocr($node->getAttribute('title'));

        return new OcrWord(new OcrBoundingBox(OcrBoundingBox::ABS, $absMinX, $abxMinY, $absMaxX, $absMaxY), $pageNumber, $parent, $node->nodeValue);

    }

}