<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;

use Officient\DocumentScan\Entity\OcrBoundingBox;
use Officient\DocumentScan\Entity\OcrPage;

class OcrPageFactory implements OcrPageFactoryInterface
{
    private OcrAreaFactoryInterface $ocrAreaFactory;

    /**
     * OcrPageFactory constructor.
     * @param OcrAreaFactoryInterface $ocrAreaFactory
     */
    public function __construct(OcrAreaFactoryInterface $ocrAreaFactory)
    {
        $this->ocrAreaFactory = $ocrAreaFactory;
    }

    /**
     * @param DOMElement $node
     * @param int $pageNumber
     * @param string $mimeType
     * @param string $image
     * @return OcrPage
     */
    public function make(DOMElement $node, int $pageNumber, string $mimeType, string $image): OcrPage
    {
        list($absMinX, $abxMinY, $absMaxX, $absMaxY) = OcrBoundingBox::getBoxCoordinatesFromHocr($node->getAttribute('title'));
        $page = new OcrPage(new OcrBoundingBox(OcrBoundingBox::ABS, $absMinX, $abxMinY, $absMaxX, $absMaxY), $pageNumber, $mimeType, $image);

        /** @var DOMElement $childNode */
        foreach ($node->childNodes as $childNode) {
            if ($childNode->nodeType == 1 and $childNode->getAttribute('class') == 'ocr_carea') {
                $page->addChild($this->ocrAreaFactory->make($childNode, $pageNumber, $page));
            }
        }
        return $page;
    }
}