<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;

use Officient\DocumentScan\Entity\OcrObject;
use Officient\DocumentScan\Entity\OcrArea;
use Officient\DocumentScan\Entity\OcrBoundingBox;

class OcrAreaFactory implements OcrAreaFactoryInterface
{
    private OcrParFactoryInterface $ocrParFactory;

    /**
     * OcrAreaFactory constructor.
     * @param OcrParFactoryInterface $ocrParFactory
     */
    public function __construct(OcrParFactoryInterface $ocrParFactory)
    {
        $this->ocrParFactory = $ocrParFactory;
    }

    /**
     * @param DOMElement $node
     * @param int $pageNumber
     * @param OcrObject $parent
     * @return OcrArea
     */
    public function make(DOMElement $node, int $pageNumber, OcrObject $parent) : OcrArea
    {
        list($absMinX, $abxMinY, $absMaxX, $absMaxY) = OcrBoundingBox::getBoxCoordinatesFromHocr($node->getAttribute('title'));
        $area = new OcrArea(new OcrBoundingBox(OcrBoundingBox::ABS, $absMinX, $abxMinY, $absMaxX, $absMaxY), $pageNumber, $parent);

        /** @var DOMElement $childNode */
        foreach ($node->childNodes as $childNode) {
            if ($childNode->nodeType == 1 and $childNode->getAttribute('class') == 'ocr_par') {
                $area->addChild($this->ocrParFactory->make($childNode, $pageNumber, $area));
            }
        }

        return $area;
    }

}