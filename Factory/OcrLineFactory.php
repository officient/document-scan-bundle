<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;

use Officient\DocumentScan\Entity\OcrObject;
use Officient\DocumentScan\Entity\OcrLine;
use Officient\DocumentScan\Entity\OcrBoundingBox;

class OcrLineFactory implements OcrLineFactoryInterface
{
    private OcrWordFactoryInterface $ocrWordFactory;

    /**
     * OcrLineFactory constructor.
     * @param OcrWordFactoryInterface $ocrWordFactory
     */
    public function __construct(OcrWordFactoryInterface $ocrWordFactory)
    {
        $this->ocrWordFactory = $ocrWordFactory;
    }


    public function make(DOMElement $node, int $pageNumber, OcrObject $parent): OcrLine
    {
        list($absMinX, $abxMinY, $absMaxX, $absMaxY) = OcrBoundingBox::getBoxCoordinatesFromHocr($node->getAttribute('title'));
        $line = new OcrLine(new OcrBoundingBox(OcrBoundingBox::ABS, $absMinX, $abxMinY, $absMaxX, $absMaxY), $pageNumber, $parent);

        /** @var DOMElement $childNode */
        $ocrWords = [];
        foreach ($node->childNodes as $childNode) {
            if ($childNode->nodeType == 1 and $childNode->getAttribute('class') == 'ocrx_word') {
                $ocrWords[] = $this->ocrWordFactory->make($childNode, $pageNumber, $line);
            }
        }

        $words = [];
        foreach ($ocrWords as $i => $ocrWord) {
            $words[] = $ocrWord->getContent();
            $line->addChild($ocrWord);
        }
        $line->setContent(implode(' ', $words));

        return $line;
    }

}