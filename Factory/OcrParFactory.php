<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;

use Officient\DocumentScan\Entity\OcrObject;
use Officient\DocumentScan\Entity\OcrPar;
use Officient\DocumentScan\Entity\OcrBoundingBox;

class OcrParFactory implements OcrParFactoryInterface
{
    private OcrLineFactoryInterface $ocrLineFactory;

    /**
     * OcrParFactory constructor.
     * @param OcrLineFactoryInterface $ocrLineFactory
     */
    public function __construct(OcrLineFactoryInterface $ocrLineFactory)
    {
        $this->ocrLineFactory = $ocrLineFactory;
    }

    /**
     * @param DOMElement $node
     * @param int $pageNumber
     * @param OcrObject $parent
     * @return OcrPar
     */
    public function make(DOMElement $node, int $pageNumber, OcrObject $parent): OcrPar
    {
        list($absMinX, $abxMinY, $absMaxX, $absMaxY) = OcrBoundingBox::getBoxCoordinatesFromHocr($node->getAttribute('title'));
        $par = new OcrPar(new OcrBoundingBox(OcrBoundingBox::ABS, $absMinX, $abxMinY, $absMaxX, $absMaxY), $pageNumber, $parent);

        /** @var DOMElement $childNode */
        foreach ($node->childNodes as $childNode) {
            if ($childNode->nodeType == 1 and ($childNode->getAttribute('class') == 'ocr_line' or $childNode->getAttribute('class') == 'ocr_header')) {
                $par->addChild($this->ocrLineFactory->make($childNode, $pageNumber, $par));
            }
        }

        return $par;
    }

}