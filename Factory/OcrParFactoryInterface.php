<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;
use Officient\DocumentScan\Entity\OcrObject;
use Officient\DocumentScan\Entity\OcrPar;

/**
 * Interface OcrParFactoryInterface
 * @package Officient\DocumentScan\Factory
 */
interface OcrParFactoryInterface
{
    /**
     * @param DOMElement $node
     * @param int $pageNumber
     * @param OcrObject $parent
     * @return OcrPar
     */
    public function make(DOMElement $node, int $pageNumber, OcrObject $parent) : OcrPar;
}