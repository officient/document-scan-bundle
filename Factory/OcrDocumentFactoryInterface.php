<?php

namespace Officient\DocumentScan\Factory;

use Officient\DocumentScan\Entity\OcrDocument;

/**
 * Interface OcrDocumentFactoryInterface
 * @package Officient\DocumentScan\Factory
 */
interface OcrDocumentFactoryInterface
{
    /**
     * Make an OcrDocument
     * @param string $hocrData
     * @param array $images
     * @return OcrDocument
     * @thows HocrParseFailedException
     */
    public function make(string $hocrData, array $images): OcrDocument;
}