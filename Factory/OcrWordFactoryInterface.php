<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;
use Officient\DocumentScan\Entity\OcrObject;
use Officient\DocumentScan\Entity\OcrWord;

/**
 * Interface OcrWordFactoryInterface
 * @package Officient\DocumentScan\Factory
 */
interface OcrWordFactoryInterface
{
    /**
     * @param DOMElement $node
     * @param int $pageNumber
     * @param OcrObject $parent
     * @return OcrWord
     */
    public function make(DOMElement $node, int $pageNumber, OcrObject $parent): OcrWord;
}