<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;
use Officient\DocumentScan\Entity\OcrArea;
use Officient\DocumentScan\Entity\OcrObject;

/**
 * Interface OcrAreaFactoryInterface
 * @package Officient\DocumentScan\Factory
 */
interface OcrAreaFactoryInterface
{
    /**
     * @param DOMElement $node
     * @param int $pageNumber
     * @param OcrObject $parent
     * @return OcrArea
     */
    public function make(DOMElement $node, int $pageNumber, OcrObject $parent) : OcrArea;
}