<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;
use Officient\DocumentScan\Entity\OcrObject;
use Officient\DocumentScan\Entity\OcrLine;

/**
 * Interface OcrLineFactoryInterface
 * @package Officient\DocumentScan\Factory
 */
interface OcrLineFactoryInterface
{
    /**
     * @param DOMElement $node
     * @param int $pageNumber
     * @param OcrObject $parent
     * @return OcrLine
     */
    public function make(DOMElement $node, int $pageNumber, OcrObject $parent) : OcrLine;
}