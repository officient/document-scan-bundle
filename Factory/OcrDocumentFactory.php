<?php

namespace Officient\DocumentScan\Factory;

use DOMDocument;
use DOMXPath;

use Officient\DocumentScan\Entity\OcrDocument;
use Officient\DocumentScan\Exception\HocrParseFailedException;


class OcrDocumentFactory implements OcrDocumentFactoryInterface
{
    private OcrPageFactoryInterface $ocrPageFactory;

    /**
     * OcrDocumentFactory constructor.
     * @param OcrPageFactoryInterface $ocrPageFactory
     */
    public function __construct(OcrPageFactoryInterface $ocrPageFactory)
    {
        $this->ocrPageFactory = $ocrPageFactory;
    }


    /**
     * @param string $hocrData
     * @param array $images
     * @return OcrDocument
     * @throws HocrParseFailedException
     */
    public function make(string $hocrData, array $images): OcrDocument
    {

        if (empty($hocrData)) {
            throw new HocrParseFailedException('No HOCR data supplied');
        }

        if (empty($images)) {
            throw new HocrParseFailedException('No images supplied');
        }

        $doc = new DOMDocument();

        $parseLog = [];
        libxml_use_internal_errors(true);
        $res = $doc->loadXML($hocrData);
        if ($res === false) {
            $parseLog[] = 'Error while loading HOCR data';
            foreach (libxml_get_errors() as $e) {
                $parseLog[] = $e->message;
            }
            $res = $doc->loadXML($this->stripInvalidXml($hocrData));
        }
        libxml_use_internal_errors(false);

        if ($res === false) {
            throw new HocrParseFailedException(implode(PHP_EOL, $parseLog));
        }

        $xpath = new DOMXpath($doc);
        if (!is_object($xpath)) {
            throw new HocrParseFailedException('Could not create XPath processor');
        }

        $xpath->registerNamespace('h', $doc->documentElement->namespaceURI);
        $pagenodes = $xpath->query("//h:html/h:body/h:div[@class='ocr_page']");
        if (empty($pagenodes)) {
            throw new HocrParseFailedException('Could not find page data in HOCR');
        }

        $document = new OcrDocument($hocrData);
        $pageNumber = 0;
        /** @var @var DOMElement $pagenode */
        foreach ($pagenodes as $pageNode) {
            $mimeType = key($images[$pageNumber]);
            $image = $images[$pageNumber][$mimeType];
            $pageNumber++;
            $document->addPage($this->ocrPageFactory->make($pageNode, $pageNumber, $mimeType, $image));
        }

        return $document;
    }


    private function stripInvalidXml($value)
    {
        $ret = "";
        //$current;
        if (empty($value)) {
            return $ret;
        }

        $length = strlen($value);
        for ($i = 0; $i < $length; $i++) {
            $current = ord($value[$i]);
            if (($current == 0x9) ||
                ($current == 0xA) ||
                ($current == 0xD) ||
                (($current >= 0x20) && ($current <= 0xD7FF)) ||
                (($current >= 0xE000) && ($current <= 0xFFFD)) ||
                (($current >= 0x10000) && ($current <= 0x10FFFF))) {
                $ret .= chr($current);
            } else {
                $ret .= " ";
            }
        }
        return $ret;
    }

}