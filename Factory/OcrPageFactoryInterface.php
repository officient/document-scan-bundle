<?php

namespace Officient\DocumentScan\Factory;

use DOMElement;
use Officient\DocumentScan\Entity\OcrPage;

/**
 * Interface OcrPageFactoryInterface
 * @package Officient\DocumentScan\Factory
 */
interface OcrPageFactoryInterface
{
    /**
     * @param DOMElement $node
     * @param int $pageNumber
     * @param string $mimeType
     * @param string $image
     * @return OcrPage
     */
    public function make(DOMElement $node, int $pageNumber, string $mimeType, string $image): OcrPage;
}