<?php

namespace Officient\DocumentScan\Manager;

use Officient\DocumentScan\Entity\OcrDocument;

/**
 * Interface OcrManagerInterface
 * @package Officient\DocumentScan\Manager
 */
interface OcrManagerInterface
{
    /**
     * @param string $documentData
     * @param string $mimeType
     * @return \Officient\DocumentScan\Entity\OcrDocument
     */
    public function processDoccumentData(string $documentData, string $mimeType): OcrDocument;
}