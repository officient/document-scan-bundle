<?php

namespace Officient\DocumentScan\Manager;

use Officient\DocumentScan\Service\OcrServiceInterface;
use Officient\DocumentScan\Entity\OcrDocument;

/**
 * Class OcrManager
 * @package Officient\DocumentScan\Manager
 */
class OcrManager implements OcrManagerInterface
{
    private OcrServiceInterface $ocrService;

    /**
     * OcrManager constructor.
     * @param OcrServiceInterface $ocrService
     */
    public function __construct(OcrServiceInterface $ocrService)
    {
        $this->ocrService = $ocrService;
    }

    /**
     * @param string $documentData
     * @param string $mimeType
     * @return OcrDocument
     */
    public function processDoccumentData(string $documentData, string $mimeType): OcrDocument
    {
        list($hocr, $images) = $this->ocrService->convertToHocr($documentData, $mimeType);
        return $this->ocrService->convertHocrToOcrDocument($hocr, $images);
    }

}