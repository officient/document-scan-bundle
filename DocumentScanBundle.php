<?php

namespace Officient\DocumentScan;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DocumentScanBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\DocumentScan
 */
class DocumentScanBundle extends Bundle
{

}