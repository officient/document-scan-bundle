<?php

namespace Officient\DocumentScan\Entity;

class OcrPage extends OcrObject
{
    private ?string $mimeType = null;
    private ?string $image = null;

    function __construct(OcrBoundingBox $absBoundingBox, int $pageNumber, string $mimeType, string $image)
    {
        parent::__construct('page', $absBoundingBox, $pageNumber, null);
        $this->setPageNumber($pageNumber);
        $this->setParent(null);
        $this->setMimeType($mimeType);
        $this->setImage($image);
    }

    /**
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param string|null $mimeType
     */
    public function setMimeType(?string $mimeType): void
    {
        $this->mimeType = $mimeType;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @param string $type
     */
    public function getAllWordsOnPage(): array
    {
        $descendants = $this->getDescendants();
        /** @param OcrObject $v */
        return array_filter($descendants, function($v) {
            return $v->getType() == OcrObject::WORD;
        });
    }

}


