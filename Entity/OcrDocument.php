<?php

namespace Officient\DocumentScan\Entity;

use DOMDocument;
use DOMXPath;

class OcrDocument
{
    protected $horc = null;
    protected $dom = null;
    protected $xpath = null;
    protected $pages = [];
    protected $parsed = false;
    protected $cachedMatchables = array();

    function __construct(string $hocr)
    {
        $this->hocr = $hocr;
    }


    /**
     * @param OcrPage $page
     */
    public function addPage(OcrPage $page)
    {
        $this->pages[] = $page;
    }
    /**
     * @return array
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @return int
     */
    function getNumberOfPages()
    {
        return count($this->getPages());
    }

    /**
     * @param int $pageNumber
     * @return OcrPage
     */
    public function getPage(int $pageNumber): OcrPage
    {
        return $this->pages[$pageNumber - 1];
    }


    /**
     * @param OcrBoundingBox $boundingBox
     * @param int|null $pageNumber
     * @return array
     */
    public function getMatchables(int $pageNumber = null, OcrBoundingBox $boundingBox = null): array
    {
        if (!$this->parsePdf()) {
            return [];
        }

        if ($boundingBox === null) {
            $boundingBox = new OcrBoundingBox(OcrBoundingBox::REL, 0.0, 0.0, 1.0, 1.0);
        }

        if ($pageNumber == null) {
            $fromPage = 1;
            $toPage = $this->getNumberOfPages();
        } else {
            $fromPage = $pageNumber;
            $toPage = $pageNumber;
        }

        $cacheKey = sprintf("%s_%d_%d_%f_%f_%f_%f", $boundingBox->getType(), $fromPage, $toPage, $boundingBox->getMinX(), $boundingBox->getMinY(), $boundingBox->getMaxX(), $boundingBox->getMaxY());

        if (isset($this->cachedMatchables[$cacheKey])) {
            return $this->cachedMatchables[$cacheKey];
        }

        $matchables = array();
        for ($i = $fromPage; $i <= $toPage; $i++) {
            $matchables = array_merge($matchables, $this->getPage($i)->getMatchables($boundingBox));
        }
        $this->cachedMatchables[$cacheKey] = $matchables;

        return $matchables;
    }


    /**
     * @param OcrObject $ocrObject
     * @param OcrBoundingBox|null $boundingBox
     * @return array
     */
    public function getMatchablesToTheRight(
        OcrObject $ocrObject,
        OcrBoundingBox $boundingBox = null
    ): array
    {
        if (!$this->parsePdf()) {
            return [];
        }

        if ($boundingBox === null) {
            $localBoundingBox = new OcrBoundingBox(OcrBoundingBox::REL, $ocrObject->getElementRelMaxX(), 0.0, 1.0, 1.0);
        } else {
            $localBoundingBox = new OcrBoundingBox($boundingBox->getType(),
                (($boundingBox->getType() == OcrBoundingBox::REL) ? $ocrObject->getElementRelMaxX() : $ocrObject->getElementAbsMaxX()),
                $boundingBox->getMinY(),
                $boundingBox->getMaxX(),
                $boundingBox->getMaxY());
        }

        $pageNumber = $ocrObject->getPageNumber();

        $cacheKey = sprintf("RIGHT_%s_%d_%d_%f_%f_%f_%f", OcrBoundingBox::REL, $pageNumber, $pageNumber, $ocrObject->getElementRelMinX(), $ocrObject->getElementRelMinY(), $ocrObject->getElementRelMaxX(), $ocrObject->getElementRelMaxY());

        if (isset($this->cachedMatchables[$cacheKey])) {
            return $this->cachedMatchables[$cacheKey];
        }

        $this->cachedMatchables[$cacheKey] = $this->getPage($pageNumber)->getMatchablesToTheRight($ocrObject, $localBoundingBox);

        return $this->cachedMatchables[$cacheKey];
    }


    /**
     * @param OcrObject $ocrObject
     * @param OcrBoundingBox|null $boundingBox
     * @return array
     */
    public function getLineToTheRight(
        OcrObject $ocrObject,
        OcrBoundingBox $boundingBox = null
    ): array
    {
        if (!$this->parsePdf()) {
            return [];
        }

        if ($boundingBox === null) {
            $localBoundingBox = new OcrBoundingBox(OcrBoundingBox::REL, $ocrObject->getElementRelMaxX(), 0.0, 1.0, 1.0);
        } else {
            $localBoundingBox = new OcrBoundingBox($boundingBox->getType(),
                (($boundingBox->getType() == OcrBoundingBox::REL) ? $ocrObject->getElementRelMaxX() : $ocrObject->getElementAbsMaxX()),
                $boundingBox->getMinY(),
                $boundingBox->getMaxX(),
                $boundingBox->getMaxY());
        }

        $pageNumber = $ocrObject->getPageNumber();

        $cacheKey = sprintf("RIGHT_%s_%d_%d_%f_%f_%f_%f", OcrBoundingBox::REL, $pageNumber, $pageNumber, $ocrObject->getElementRelMinX(), $ocrObject->getElementRelMinY(), $ocrObject->getElementRelMaxX(), $ocrObject->getElementRelMaxY());

        if (isset($this->cachedMatchables[$cacheKey])) {
            return $this->cachedMatchables[$cacheKey];
        }

        $this->cachedMatchables[$cacheKey] = $this->getPage($pageNumber)->getLineToTheRight($ocrObject, $localBoundingBox);

        return $this->cachedMatchables[$cacheKey];
    }


    /**
     * @param OcrBoundingBox|null $boundingBox
     * @return array
     */
    public function getLines(int $pageNumber,
                             OcrBoundingBox $boundingBox = null
    ): array
    {
        if (!$this->parsePdf()) {
            return [];
        }

        if ($boundingBox === null) {
            $localBoundingBox = new OcrBoundingBox(OcrBoundingBox::REL, 0.0, 0.0, 1.0, 1.0);
        } else {
            $localBoundingBox = new OcrBoundingBox($boundingBox->getType(),
                $boundingBox->getMinX(),
                $boundingBox->getMinY(),
                $boundingBox->getMaxX(),
                $boundingBox->getMaxY());
        }

        $cacheKey = sprintf("LINES_%s_%d_%d_%f_%f_%f_%f", $localBoundingBox->getType(), $pageNumber, $pageNumber, $localBoundingBox->getMinX(), $localBoundingBox->getMinY(), $localBoundingBox->getMaxX(), $localBoundingBox->getMaxY());

        if (isset($this->cachedMatchables[$cacheKey])) {
            return $this->cachedMatchables[$cacheKey];
        }

        $this->cachedMatchables[$cacheKey] = $this->getPage($pageNumber)->getLines($localBoundingBox);

        return $this->cachedMatchables[$cacheKey];
    }


    // page: which page to search
    // boundingbox: which area to search
    // search: array with match
    //        'match' => 'expression'
    //        'type'  => 'string'|'line'|'regex'
    public function findMatches(array $matchables, array $search)
    {
        $matches = array();

        /** @var OcrObject $matchable */
        foreach ($matchables as $matchable) {
            switch ($search['type']) {
                case 'string':
                    if ($matchable->getContent() == $search['match']) {
                        $matches[] = $matchable;
                    }
                    break;
                case 'regex':
                    $preg_res = preg_match($search['regex'], $matchable->getContent(), $regmatch);
                    if ($preg_res == 1) {
                        $matches[] = $matchable;
                    }
                    break;
                default:
                    return $matches;
            }
        }
        return $matches;
    }


    public function findTags(array $tagSearch, int $pageNumber = null, OcrBoundingBox $boundingBox = null)
    {
        $tagMatchables = $this->getMatchables($pageNumber, $boundingBox);

        $tagMatches = $this->findMatches($tagMatchables, $tagSearch);
        return $tagMatches;
    }


    public function findTaggedValues(array $tagSearch, string $direction, array $valueSearch, int $pageNumber = null, OcrBoundingBox $boundingBox = null)
    {
        $tagMatchables = $this->getMatchables($pageNumber, $boundingBox);

        $tagMatches = $this->findMatches($tagMatchables, $tagSearch);
        $foundTaggedValues = array();
        foreach ($tagMatches as $tagMatch) {
            switch ($direction) {
                case OcrObject::RIGHT:
                    if ($valueSearch['type'] == 'line') {
                        $valueMatch = $this->getLineToTheRight($tagMatch, $boundingBox);
                        if (!empty($valueMatch)) {
                            $foundTaggedValues[] = ['tag' => $tagMatch, 'values' => $valueMatch];
                        }
                    } else {
                        $valueMatchables = $this->getMatchablesToTheRight($tagMatch, $boundingBox);
                        $valueMatches = $this->findMatches($valueMatchables, $valueSearch);
                        if (count($valueMatches) > 0) {
                            $foundTaggedValues[] = ['tag' => $tagMatch, 'values' => $valueMatches];
                        }
                    }
                    break;
                case OcrObject::LEFT:
                    return array();
                    break;
                case OcrObject::ABOVE:
                    return array();
                    break;
                case OcrObject::BELOW:
                    return array();
                    break;
                default:
                    // todo: should probably throw an exception here instead
                    return array();
            }

        }
        return $foundTaggedValues;
    }

    /**
     * @param array $tagSearch
     * @param string $direction
     * @param array $valueSearch
     * @param int|null $pageNumber
     * @param OcrBoundingBox|null $boundingBox
     * @return array
     */
    public function findTaggedValue(array $tagSearch, string $direction, array $valueSearch, int $pageNumber = null, OcrBoundingBox $boundingBox = null)
    {
        if ($this->parsePdf()) {
            $foundTaggedValues = $this->findTaggedValues($tagSearch, $direction, $valueSearch, $pageNumber, $boundingBox);
            if (count($foundTaggedValues) > 0) {
                $taggedValue = ['tag' => $foundTaggedValues[0]['tag'], 'value' => $foundTaggedValues[0]['values'][0]];
                if (isset($valueSearch['select'])) {
                    switch ($valueSearch['select']) {
                        case 'longest-value':
                            foreach ($foundTaggedValues as $foundTaggedValue) {
                                foreach ($foundTaggedValue['values'] as $value) {
                                    if (strlen($value->getContent()) > strlen($taggedValue['value']->getContent())) {
                                        $taggedValue = ['tag' => $foundTaggedValue['tag'], 'value' => $value];
                                    }
                                }
                            }
                            break;
                        case 'shortest-value':
                            break;
                    }
                }
                return $taggedValue;
            }
        }
        return null;
    }

    public function getPageAndPositionAsString(OcrObject $ocrObject): string
    {
        return $ocrObject->getPageNumber() . ' rel. pos. [min X: ' . $ocrObject->getElementRelMinX() . ', min Y: ' . $ocrObject->getElementRelMinY() . ', max X: ' . $ocrObject->getElementRelMaxX() . ', max Y: ' . $ocrObject->getElementRelMaxY() . ']';
    }

    public function getPageNumber(OcrObject $ocrObject): string
    {
        return $ocrObject->getPageNumber();
    }


    public function findTagFromBottom(array $tagSearch, int $pageNumber = null, OcrBoundingBox $boundingBox = null)
    {
        $selectedTag = null;
        if ($this->parsePdf()) {
            $foundTags = $this->findTags($tagSearch, $pageNumber, $boundingBox);
            foreach ($foundTags as $foundTag) {
                if (is_null($selectedTag) or
                    ($foundTag->getPageNumber() >= $selectedTag->getPageNumber()) or
                    ($foundTag->getPageNumber() == $selectedTag->getPageNumber() and $foundTag->getMinRelY() >= $selectedTag->getMinRelY())) {
                    $selectedTag = $foundTag;
                }
            }
        }
        return $selectedTag;
    }


    public function findTagBelow(array $tagSearch, OcrObject $ocrObject, OcrBoundingBox $boundingBox = null)
    {
        $selectedTag = null;
        if ($this->parsePdf()) {
            $startpage = $currentpage = $ocrObject->getPageNumber();
            $endpage = $this->getNumberOfPages();
            while (is_null($selectedTag) and $currentpage <= $endpage) {
                if ($currentpage == $startpage) {
                    $foundTags = $this->findTags($tagSearch, $currentpage, new OcrBoundingBox(OcrBoundingBox::REL, 0.0, $ocrObject->getElementRelMaxY(), 1.0, 1.0));
                } else {
                    $foundTags = $this->findTags($tagSearch, $currentpage, new OcrBoundingBox(OcrBoundingBox::REL, 0.0, 0.0, 1.0, 1.0));
                }
                foreach ($foundTags as $foundTag) {
                    if ($foundTag->isBelow($ocrObject) and $foundTag->horizontalOverlap($ocrObject) > 0.5) {
                        if (is_null($selectedTag) or
                            ($foundTag->getMinRelY() < $selectedTag->getMinRelY())) {
                            $selectedTag = $foundTag;
                        }
                    }
                }
                $currentpage += 1;
            }
        }
        return $selectedTag;
    }

    public function findTagInArea(array $tagSearch, $startPage, $endPage, $searchDirection = 'DOWN', $startBoundingBox = null, $endBoundingBox = null, $boundingBox = null)
    {
        if (is_null($startBoundingBox)) {
            $startBoundingBox = new OcrBoundingBox(OcrBoundingBox::REL, 0.0, 0.0, 1.0, 1.0);
        }
        if (is_null($endBoundingBox)) {
            $endBoundingBox = new OcrBoundingBox(OcrBoundingBox::REL, 0.0, 0.0, 1.0, 1.0);
        }
        if (is_null($boundingBox)) {
            $boundingBox = new OcrBoundingBox(OcrBoundingBox::REL, 0.0, 0.0, 1.0, 1.0);
        }

        $selectedTag = null;
        if ($this->parsePdf()) {
            $currentpage = $startPage;
            while (is_null($selectedTag) and $currentpage <= $endPage) {
                switch ($currentpage) {
                    case $startPage:
                        $foundTags = $this->findTags($tagSearch, $currentpage, $startBoundingBox);
                        break;
                    case $endPage:
                        $foundTags = $this->findTags($tagSearch, $currentpage, $endBoundingBox);
                        break;
                    default:
                        $foundTags = $this->findTags($tagSearch, $currentpage, $boundingBox);
                        break;
                }
                foreach ($foundTags as $foundTag) {
                    if (is_null($selectedTag) or
                        ($searchDirection == 'DOWN' and $foundTag->getMinRelY() < $selectedTag->getMinRelY()) or
                        ($searchDirection == 'UP' and $foundTag->getMaxRelY() > $selectedTag->getMaxRelY())) {
                        $selectedTag = $foundTag;
                    }
                }
                $currentpage += 1;
            }
        }
        return $selectedTag;
    }


    public
    function findTaggedValueFromTop(array $tagSearch, string $direction, array $valueSearch, int $pageNumber = null, OcrBoundingBox $boundingBox = null)
    {
        $selectedTaggedValue = null;
        if ($this->parsePdf()) {
            $foundTaggedValues = $this->findTaggedValues($tagSearch, $direction, $valueSearch, $pageNumber, $boundingBox);
            foreach ($foundTaggedValues as $foundTaggedValue) {
                if (is_null($selectedTaggedValue) or
                    ($foundTaggedValue['tag']->getPageNumber() < $selectedTaggedValue['tag']->getPageNumber()) or
                    ($foundTaggedValue['tag']->getPageNumber() == $selectedTaggedValue['tag']->getPageNumber() and $foundTaggedValue['tag']->getMinRelY() < $selectedTaggedValue['tag']->getMinRelY())) {
                    $selectedTaggedValue = ['tag' => $foundTaggedValue['tag'], 'value' => $foundTaggedValue['values'][0]];
                }
            }
        }
        return $selectedTaggedValue;
    }

    public
    function findTaggedValueFromBottom(array $tagSearch, string $direction, array $valueSearch, int $pageNumber = null, OcrBoundingBox $boundingBox = null)
    {
        $selectedTaggedValue = null;
        if ($this->parsePdf()) {
            $foundTaggedValues = $this->findTaggedValues($tagSearch, $direction, $valueSearch, $pageNumber, $boundingBox);
            foreach ($foundTaggedValues as $foundTaggedValue) {
                if (is_null($selectedTaggedValue) or
                    ($foundTaggedValue['tag']->getPageNumber() >= $selectedTaggedValue['tag']->getPageNumber()) or
                    ($foundTaggedValue['tag']->getPageNumber() == $selectedTaggedValue['tag']->getPageNumber() and $foundTaggedValue['tag']->getMinRelY() >= $selectedTaggedValue['tag']->getMinRelY())) {
                    $selectedTaggedValue = ['tag' => $foundTaggedValue['tag'], 'value' => $foundTaggedValue['values'][0]];
                }
            }
        }
        return $selectedTaggedValue;
    }

    public
    function findTaggedValueAbove(array $tagSearch, string $direction, array $valueSearch, OcrObject $ocrObject, OcrBoundingBox $boundingBox = null)
    {
        $selectedTaggedValue = null;
        if ($this->parsePdf()) {
            $foundTaggedValues = $this->findTaggedValues($tagSearch, $direction, $valueSearch, null, $boundingBox);
            foreach ($foundTaggedValues as $foundTaggedValue) {
                if ($foundTaggedValue['tag']->isAbove($ocrObject) and $foundTaggedValue['tag']->horizontalOverlap($ocrObject) > 0.5) {
                    if (is_null($selectedTaggedValue) or
                        ($foundTaggedValue['tag']->getPageNumber() >= $selectedTaggedValue['tag']->getPageNumber()) or
                        ($foundTaggedValue['tag']->getPageNumber() == $selectedTaggedValue['tag']->getPageNumber() and $foundTaggedValue['tag']->getMinRelY() >= $selectedTaggedValue['tag']->getMinRelY())) {
                        $selectedTaggedValue = ['tag' => $foundTaggedValue['tag'], 'value' => $foundTaggedValue['values'][0]];
                    }
                }
            }
        }
        return $selectedTaggedValue;
    }

    public function findMatchToTheRightOf(array $valueSearch, OcrObject $ocrObject, OcrBoundingBox $boundingBox = null)
    {
        $valueMatchables = $this->getMatchablesToTheRight($ocrObject, $boundingBox);

        $foundValue = null;
        $valueMatches = $this->findMatches($valueMatchables, $valueSearch);
        foreach ($valueMatches as $valueMatch) {
            if (is_null($foundValue) or
                ($valueMatch->getMinRelX() < $foundValue->getMinRelX())) {
                $foundValue = $valueMatch;
            }

        }
        return $foundValue;
    }
}


