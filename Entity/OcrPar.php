<?php

namespace Officient\DocumentScan\Entity;


class OcrPar extends OcrObject
{
    function __construct(OcrBoundingBox $absBoundingBox, int $pageNumber, OcrObject $parent)
    {
        parent::__construct('par', $absBoundingBox, $pageNumber, $parent);
    }
}


