<?php

namespace Officient\DocumentScan\Entity;

class OcrObject
{
    public const PAGE = 'page';       // hocr div class='ocr_page' - pdftotext page element
    public const AREA = 'area';       // hocr div class='ocr_carea' - pdftotext flow
    public const PAR = 'par';         // hocr p class='ocr_par' - pdftotext block
    public const LINE = 'line';       // hocr span class='ocr_line' - pdftotext line
    public const SUBLINE = 'subline';
    public const WORD = 'word';       // hocr span class='ocrx_

    const RIGHT = 'right';
    const LEFT = 'left';
    const BELOW = 'below';
    const ABOVE = 'above';

    protected ?string $type = null;
    protected ?OcrBoundingBox $pageAbsBoundingBox = null;
    protected ?OcrBoundingBox $pageRelBoundingBox = null;
    protected ?OcrBoundingBox$elementAbsBoundingBox = null;
    protected ?OcrBoundingBox$elementRelBoundingBox = null;
    protected ?int $pageNumber = null;
    protected ?OcrObject $parent = null;
    protected array $children = array();
    //   protected $content = null;
    protected array $cachedMatchables = array();
    protected string $content = '';

    function __construct(string $type, OcrBoundingBox $absBoundingBox, int $pageNumber, OcrObject $parent = null)
    {
        $this->setType($type);
        $this->setPageNumber($pageNumber);
        $this->setElementAbsBoundingBox($absBoundingBox);
        $this->setParent($parent);
    }


    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getElementAbsMinX(): float // was getMinX
    {
        return $this->getElementAbsBoundingBox()->getMinX();
    }

    /**
     * @return float
     */
    public function getElementAbsMinY(): float
    {
        return $this->getElementAbsBoundingBox()->getMinY();
    }

    /**
     * @return float
     */
    public function getElementAbsMaxX(): float
    {
        return $this->getElementAbsBoundingBox()->getMaxX();
    }

    /**
     * @return float
     */
    public function getElementAbsMaxY(): float
    {
        return $this->getElementAbsBoundingBox()->getMaxY();
    }

    /**
     * @return float
     */
    public function getElementRelMinX(): float // was getMinRelX
    {
        return $this->getElementRelBoundingBox()->getMinX();
    }

    /**
     * @return float
     */
    public function getElementRelMinY(): float
    {
        return $this->getElementRelBoundingBox()->getMinY();
    }

    /**
     * @return float
     */
    public function getElementRelMaxX(): float
    {
        return $this->getElementRelBoundingBox()->getMaxX();
    }

    /**
     * @return float
     */
    public function getElementRelMaxY(): float
    {
        return $this->getElementRelBoundingBox()->getMaxY();
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        if (is_null($this->pageNumber) and !is_null($this->getParent())) {
            return $this->getParent()->getPageNumber();
        } else {
            return $this->pageNumber;
        }
    }

    /**
     * @param int $pageNumber
     */
    public function setPageNumber(int $pageNumber)
    {
        $this->pageNumber = $pageNumber;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param OcrObject $child
     */
    public function addChild(OcrObject $child)
    {
        $child->setParent($this);
        $this->children[] = $child;
    }

    /**
     * @return OcrBoundingBox
     */
    public function getPageAbsBoundingBox(): OcrBoundingBox
    {
        if (empty($this->pageAbsBoundingBox)) {
            if ($this->$this)
                $this->setPageAbsBoundingBox($this->getParent()->getPageAbsBoundingBox());
        }
        return $this->pageAbsBoundingBox;
    }

    /**
     * @param OcrBoundingBox $pageAbsBoundingBox
     */
    public function setPageAbsBoundingBox(OcrBoundingBox $pageAbsBoundingBox): void
    {
        $this->pageAbsBoundingBox = $pageAbsBoundingBox;
    }

    /**
     * @return OcrBoundingBox
     */
    public function getPageRelBoundingBox(): OcrBoundingBox
    {
        if (empty($this->pageRelBoundingBox)) {
            $this->setElementRelBoundingBox(new OcrBoundingBox(OcrBoundingBox::REL, 0.0, 0.0, 1.0, 1.0));
        }
        return $this->pageRelBoundingBox;
    }

    /**
     * @param OcrBoundingBox $pageRelBoundingBox
     */
    public function setPageRelBoundingBox(OcrBoundingBox $pageRelBoundingBox): void
    {
        $this->pageRelBoundingBox = $pageRelBoundingBox;
    }

    /**
     * @return OcrBoundingBox
     */
    public function getElementAbsBoundingBox(): OcrBoundingBox
    {
        return $this->elementAbsBoundingBox;
    }

    /**
     * @param OcrBoundingBox $elementAbsBoundingBox
     */
    public function setElementAbsBoundingBox(OcrBoundingBox $elementAbsBoundingBox): void
    {
        $this->elementAbsBoundingBox = $elementAbsBoundingBox;
    }

    /**
     * @return OcrBoundingBox
     */
    public function getElementRelBoundingBox(): OcrBoundingBox
    {
        if (empty($this->elementRelBoundingBox)) {
            $pageAbsMinX = $this->getPageAbsBoundingBox()->getMinX();
            $pageAbsMinY = $this->getPageAbsBoundingBox()->getMinY();
            $pageAbsMaxX = $this->getPageAbsBoundingBox()->getMaxX();
            $pageAbsMaxY = $this->getPageAbsBoundingBox()->getMaxY();
            $elementAbsMinX = $this->getElementAbsBoundingBox()->getMinX();
            $elementAbsMinY = $this->getElementAbsBoundingBox()->getMinY();
            $elementAbsMaxX = $this->getElementAbsBoundingBox()->getMaxX();
            $elementAbsMaxY = $this->getElementAbsBoundingBox()->getMaxY();
            $elementRelMinX = ($elementAbsMinX - $pageAbsMinX) / ($pageAbsMaxX - $pageAbsMinX);
            $elementRelMinY = ($elementAbsMinY - $pageAbsMinY) / ($pageAbsMaxY - $pageAbsMinY);
            $elementRelMaxX = ($elementAbsMaxX - $pageAbsMinX) / ($pageAbsMaxX - $pageAbsMinX);
            $elementRelMaxY = ($elementAbsMaxY - $pageAbsMinY) / ($pageAbsMaxY - $pageAbsMinY);
            $this->setElementRelBoundingBox(new OcrBoundingBox(OcrBoundingBox::REL, $elementRelMinX, $elementRelMinY, $elementRelMaxX, $elementRelMaxY));
        }
        return $this->elementRelBoundingBox;
    }

    /**
     * @param OcrBoundingBox $elementRelBoundingBox
     */
    public function setElementRelBoundingBox(OcrBoundingBox $elementRelBoundingBox): void
    {
        $this->elementRelBoundingBox = $elementRelBoundingBox;
    }

    /**
     * @return array
     */
    public function getDescendants(): array
    {
        if ($this->getType() == 'word') {
            return [];
        }
        $descendants = $this->getChildren();
        foreach ($this->getChildren() as $child) {
            foreach ($child->getDescendants() as $k => $v) {
                array_push($descendants, $v);
            }
        }
        return $descendants;
    }

    // ------------------------

    /**
     * @param OcrBoundingBox $boundingBox
     * @return bool
     */
    public function isInside(OcrBoundingBox $boundingBox): bool
    {
        if ($boundingBox->getType() == 'absolute') {
            if ($this->getElementAbsMinX() >= $boundingBox->getMinX() and $this->getElementAbsMinY() >= $boundingBox->getMinY() and $this->getElementAbsMaxX() <= $boundingBox->getMaxX() and $this->getElementAbsMaxY() <= $boundingBox->getMaxY()) {
                return true;
            }
        } else {
            if ($this->getElementRelMinX() >= $boundingBox->getMinX() and $this->getElementRelMinY() >= $boundingBox->getMinY() and $this->getElementRelMaxX() <= $boundingBox->getMaxX() and $this->getElementRelMaxY() <= $boundingBox->getMaxY()) {
                return true;
            }
        }
        return false;
    }


    public function verticalOverlap(OcrObject $ocrObject)
    {
        // check for vertical overlap (must be positive)
        $dy = (min(array($this->getElementAbsMaxY(), $ocrObject->getElementAbsMaxY())) - max(array(
                    $this->getElementAbsMinY(),
                    $ocrObject->getElementAbsMinY()
                ))) / min(array(
                $this->getElementAbsMaxY() - $this->getElementAbsMinY(),
                $ocrObject->getElementAbsMaxY() - $ocrObject->getElementAbsMinY()
            ));

        if ($dy > 0) {
            return $dy;
        } else {
            return 0.0;
        }
    }

    public function horizontalOverlap(OcrObject $ocrObject)
    {
        // check for horizontal overlap (must be positive)
        $dx = (min(array($this->getElementAbsMaxX(), $ocrObject->getElementAbsMaxX())) - max(array(
                    $this->getElementAbsMinX(),
                    $ocrObject->getElementAbsMinX()
                ))) / min(array(
                $this->getElementAbsMaxX() - $this->getElementAbsMinX(),
                $ocrObject->getElementAbsMaxX() - $ocrObject->getElementAbsMinX()
            ));

        if ($dx > 0) {
            return $dx;
        } else {
            return 0.0;
        }
    }

    public function isRightOf(OcrObject $ocrObject)
    {
        if ($this->getPageNumber() == $ocrObject->getPageNumber() and $this->getElementAbsMinX() > $ocrObject->getElementAbsMaxX()) {
            return true;
        } else {
            return false;
        }
    }

    public function isLeftOf(OcrObject $ocrObject)
    {
        if ($this->getPageNumber() == $ocrObject->getPageNumber() and $this->getElementAbsMaxX() < $ocrObject->getElementAbsMinX()) {
            return true;
        } else {
            return false;
        }
    }

    public function isAbove(OcrObject $ocrObject)
    {
        if ($this->getPageNumber() < $ocrObject->getPageNumber() or
            ($this->getPageNumber() == $ocrObject->getPageNumber() and $this->getElementAbsMaxY() < $ocrObject->getElementAbsMinY())) {
            return true;
        } else {
            return false;
        }
    }

    public function isBelow(OcrObject $ocrObject)
    {
        if ($this->getPageNumber() > $ocrObject->getPageNumber() or
            ($this->getPageNumber() == $ocrObject->getPageNumber() and $this->getElementAbsMinY() > $ocrObject->getElementAbsMaxY())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param OcrBoundingBox $boundingBox
     * @return array
     */
    public function getMatchables(OcrBoundingBox $boundingBox): array
    {
        $cacheKey = sprintf("%s_%s_%d_%f_%f_%f_%f", $this->getType(), $boundingBox->getType(), $this->getPageNumber(), $boundingBox->getMinX(), $boundingBox->getMinY(), $boundingBox->getMaxX(), $boundingBox->getMaxY());

        if (isset($this->cachedMatchables[$cacheKey])) {
            return $this->cachedMatchables[$cacheKey];
        }

        $matchables = array();
        if ($this->getType() == self::WORD and $this->isInside($boundingBox)) {
            $matchables[] = $this;
        } else {
            foreach ($this->getChildren() as $child) {
                $matchables = array_merge($matchables, $child->getMatchables($boundingBox));
            }

            if ($this->getType() == self::LINE) {
                if ($this->isInside($boundingBox)) {
                    $matchables[] = $this;
                } elseif (count($matchables) > 1) {
                    $subline = new OcrSubLine($matchables, $this->getParent());
                    $matchables[] = $subline;
                }
            }
        }
        $this->cachedMatchables[$cacheKey] = $matchables;

        return $matchables;
    }


    /**
     * @param OcrObject $ocrObject
     * @param OcrBoundingBox $boundingBox
     * @return array
     */
    public function getMatchablesToTheRight(OcrObject $ocrObject, OcrBoundingBox $boundingBox = null): array
    {
        if (is_null($boundingBox)) {
            $boundingBox = new OcrBoundingBox(OcrBoundingBox::REL, $ocrObject->getElementRelMaxX(), 0.0, 1.0, 1.0);
        }
        $matchables = array();
        if ($this->getType() == self::WORD and $this->isInside($boundingBox)
            and $this->getElementAbsMinX() > $ocrObject->getElementAbsMaxX()
            and $this->verticalOverlap($ocrObject) > 0.7) {
            $matchables[] = $this;
        } else {
            foreach ($this->getChildren() as $child) {
                $matchables = array_merge($matchables, $child->getMatchablesToTheRight($ocrObject, $boundingBox));
            }

            if ($this->getType() == self::LINE) {
                if ($this->isInside($boundingBox) and $this->getElementAbsMinX() > $ocrObject->getElementAbsMaxX()
                    and $this->verticalOverlap($ocrObject) > 0.7) {
                    $matchables[] = $this;
                } elseif (count($matchables) > 1) {
                    $subline = new OcrSubLine($matchables, $this->getParent());
                    $matchables[] = $subline;
                }
            }
        }
        return $matchables;
    }

    /**
     * @param OcrObject $ocrObject
     * @param OcrBoundingBox $boundingBox
     * @return array
     */
    public function getLineToTheRight(OcrObject $ocrObject, OcrBoundingBox $boundingBox = null): array
    {
        if (is_null($boundingBox)) {
            $boundingBox = new OcrBoundingBox(OcrBoundingBox::REL, $ocrObject->getElementRelMaxX(), 0.0, 1.0, 1.0);
        }
        $matchables = array();
        if ($this->getType() == self::LINE and $this->isInside($boundingBox)
            and $this->getElementAbsMinX() > $ocrObject->getElementAbsMaxX()
            and $this->verticalOverlap($ocrObject) > 0.7) {
            $matchables[] = $this;
        } else {
            foreach ($this->getChildren() as $child) {
                $matchables = array_merge($matchables, $child->getLineToTheRight($ocrObject, $boundingBox));
            }

            if ($this->getType() == self::LINE) {
                if ($this->isInside($boundingBox) and $this->getElementAbsMinX() > $ocrObject->getElementAbsMaxX()
                    and $this->verticalOverlap($ocrObject) > 0.7) {
                    $matchables[] = $this;
                } elseif (count($matchables) > 1) {
                    $subline = new OcrSubLine($matchables, $this->getParent());
                    $matchables[] = $subline;
                }
            }
        }
        return $matchables;
    }


    /**
     * @param OcrBoundingBox $boundingBox
     * @return array
     */
    public function getLines(OcrBoundingBox $boundingBox = null): array
    {
        if (is_null($boundingBox)) {
            $boundingBox = new OcrBoundingBox(OcrBoundingBox::REL, 0.0, 0.0, 1.0, 1.0);
        }
        $matchables = array();
        if ($this->getType() == self::LINE and $this->isInside($boundingBox)) {
            $matchables[] = $this;
        } else {
            foreach ($this->getChildren() as $child) {
                $matchables = array_merge($matchables, $child->getLines($boundingBox));
            }

            if ($this->getType() == self::LINE) {
                if ($this->isInside($boundingBox)) {
                    $matchables[] = $this;
                } elseif (count($matchables) > 1) {
                    $subline = new OcrSubLine($matchables, $this->getParent());
                    $matchables[] = $subline;
                }
            }
        }
        return $matchables;
    }

}
