<?php

namespace Officient\DocumentScan\Entity;

class OcrLine extends OcrObject
{
     protected string $content;

    function __construct(OcrBoundingBox $absBoundingBox, int $pageNumber, OcrObject $parent, $content = '')
    {
        parent::__construct('line', $absBoundingBox, $pageNumber, $parent);
        $this->setContent($content);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}


