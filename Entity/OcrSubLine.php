<?php

namespace Officient\DocumentScan\Entity;


class OcrSubLine extends OcrObject
{
    function __construct(array $pdfToTextWords, OcrPar $parent)
    {
        $xMin = $yMin = $xMax = $yMax = null;

        $this->children = $pdfToTextWords;

        /** @var OcrWord $pdfToTextWords */
        $words = array();
        foreach ($pdfToTextWords as $pdfToTextWord) {
            $words[] = $pdfToTextWord->getContent();
            if (is_null($xMin) or $xMin > $pdfToTextWord->getMinX()) {
                $xMin = $pdfToTextWord->getMinX();
            }
            if (is_null($yMin) or $yMin > $pdfToTextWord->getMinY()) {
                $yMin = $pdfToTextWord->getMinY();
            }
            if (is_null($xMax) or $xMax < $pdfToTextWord->getMaxX()) {
                $xMax = $pdfToTextWord->getMaxX();
            }
            if (is_null($yMax) or $yMax < $pdfToTextWord->getMaxY()) {
                $yMax = $pdfToTextWord->getMaxY();
            }
        }

        $this->setBoundingBox(new OcrBoundingBox(OcrBoundingBox::ABS, $xMin, $yMin, $xMax, $yMax));
        $this->setContent(implode(' ', $words));
        $this->setParent($parent);
    }
}