<?php

namespace Officient\DocumentScan\Entity;

use DOMElement;

class OcrWord extends OcrObject
{
    function __construct(OcrBoundingBox $absBoundingBox, int $pageNumber, OcrObject $parent, string $content)
    {
        parent::__construct('word', $absBoundingBox, $pageNumber, $parent);
        $this->setContent($content);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

}


