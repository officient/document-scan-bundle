<?php


namespace Officient\DocumentScan\Entity;

use Exception;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Imagick;


class OcrService
{

    static function convertToHocr($mime_type, $data): array
    {
        $file_types = ['application/pdf' => 'pdf', 'image/tiff' => 'tiff', 'image/jpeg' => 'jpg', 'image/png' => 'png'];
        $files = [];
        $mime_type = strtolower($mime_type);
        try {
            $image_file = tempnam('/data/tmp', $file_types[$mime_type] . '_');
            file_put_contents($image_file, $data);
            $files[] = $image_file;
            $images = [];
            if ($mime_type == 'application/pdf') {
                $im = new Imagick();
                $im->setResolution(300, 300);
                $im->readImage($image_file);
                $im->setImageFormat('tiff');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_DEACTIVATE);
                $im->setCompression(Imagick::COMPRESSION_GROUP4);
                $mime_type = 'image/tiff';
                $image_file .= '.tiff';
                $im->writeImages($image_file, true);
                $files[] = $image_file;
                foreach ($im as $i=> $image) {
                    $image->setImageFormat('png');
                    $images[] = ['image/png' => $image->getImageBlob()];
                }

            } else {
                $images[] = [$mime_type => $data];
            }
            $tesseract = new TesseractOCR($image_file);
            #$tesseract->config("tessedit_pageseg_mode", 1);
            #$tesseract->config("tessedit_ocr_engine_mode", 1);
            $tesseract->config("tessedit_create_hocr", 1);
            #$tesseract->config("tessedit_load_sublangs",)
            $hocr_file = tempnam('/data/tmp', 'hocr_');
            $tesseract->lang("dan", "deu", "eng", "nor");
            $tesseract->configFile('hocr');
            $tesseract->setOutputFile($hocr_file);
            $hocr = $tesseract->run();
            // $hocr = file_get_contents($hocr_file);
            $files[] = $hocr_file;
            return ['OK', ucfirst($file_types[$mime_type] . ' converted to HOCR'), $hocr, $images];
        } catch (Exception $e) {
            return ['ERROR', ucfirst($file_types[$mime_type]) . ' conversion failed: ' . $e->getMessage(), null, null];
        } finally {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }

} // class OcrDocument
