<?php

namespace Officient\DocumentScan\Entity;

class OcrArea extends OcrObject
{
    function __construct(OcrBoundingBox $absBoundingBox, int $pageNumber, OcrObject $parent)
    {
        parent::__construct('area', $absBoundingBox, $pageNumber, $parent);
    }
}

