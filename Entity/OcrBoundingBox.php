<?php

namespace Officient\DocumentScan\Entity;

class OcrBoundingBox
{
    const REL = 'REL';
    const ABS = 'ABS';
    protected float $minX;
    protected float $minY;
    protected float $maxX;
    protected float $maxY;
    protected string $type;

    /**
     * OrcBoundingBox constructor.
     * @param string $type
     * @param float $minX
     * @param float $minY
     * @param float $maxX
     * @param float $maxY
     */
    public function __construct(string $type, float $minX, float $minY, float $maxX, float $maxY)
    {
        if ($minX >= $maxX or $minY >= $maxY or !in_array($type,
                [OcrBoundingBox::ABS, OcrBoundingBox::REL])) {
            return null;
        }
        $this->setMinX($minX);
        $this->setMinY($minY);
        $this->setMaxX($maxX);
        $this->setMaxY($maxY);
        $this->setType($type);
    }


    /**
     * getBoxCoordinatesFromHocr
     * @param string $hocr_bounding_box
     */
    static public function getBoxCoordinatesFromHocr(string $hocr_bounding_box): ?array
    {
        // should match
        if (1 === preg_match("/bbox\s+(?'min_x'\d+)\s+(?'min_y'\d+)\s+(?'max_x'\d+)\s+(?'max_y'\d+)/", $hocr_bounding_box, $matches)) {
            return [$matches['min_x'], $matches['min_y'], $matches['max_x'], $matches['max_y']];
        }
        return null;
    }

    /**
     * @return float
     */
    public function getMinX() : float
    {
        return $this->minX;
    }

    /**
     * @param float $minX
     */
    public function setMinX(float $minX)
    {
        $this->minX = $minX;
    }

    /**
     * @return float
     */
    public function getMinY() : float
    {
        return $this->minY;
    }

    /**
     * @param float $minY
     */
    public function setMinY(float $minY)
    {
        $this->minY = $minY;
    }

    /**
     * @return float
     */
    public function getMaxX() : float
    {
        return $this->maxX;
    }

    /**
     * @param float $maxX
     */
    public function setMaxX(float $maxX)
    {
        $this->maxX = $maxX;
    }

    /**
     * @return float
     */
    public function getMaxY() : float
    {
        return $this->maxY;
    }

    /**
     * @param float $maxY
     */
    public function setMaxY(float $maxY)
    {
        $this->maxY = $maxY;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getHeight() : float
    {
        return $this->getMaxY() - $this->getMinY();
    }

    /**
     * @return float
     */
    public function getWidth() : float
    {
        return $this->getMaxX() - $this->getMinX();
    }

}