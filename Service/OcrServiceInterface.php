<?php

namespace Officient\DocumentScan\Service;

use Officient\DocumentScan\Exception\HocrCreationFailedException;
use Officient\DocumentScan\Entity\OcrDocument;

interface OcrServiceInterface
{
    /**
     * @param string $mimeType
     * @param string $DocumentData
     * @return array
     * @throws HocrCreationFailedException
     */
    public function convertToHocr(string $mimeType, string $DocumentData): array;

    /**
     * @param string $hocr
     * @param array $images
     * @return OcrDocument
     */
    public function convertHocrToOcrDocument(string $hocr, array $images) : OcrDocument;

    /**
     * @return string
     */
    public function getWorkDir(): string;

    /**
     * @param string $workDir
     */
    public function setWorkDir(string $workDir): void;

}
