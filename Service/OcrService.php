<?php


namespace Officient\DocumentScan\Service;

use Exception;
use Officient\DocumentScan\Exception\HocrCreationFailedException;
use Officient\DocumentScan\Entity\OcrDocument;
use Officient\DocumentScan\Factory\OcrDocumentFactoryInterface;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Imagick;


class OcrService implements OcrServiceInterface
{
    private OcrDocumentFactoryInterface $ocrDocumentFactory;
    private string $workDir;

    /**
     * OcrService constructor.
     * @param string $ocrWorkDirectory
     * @param OcrDocumentFactoryInterface $ocrDocumentFactory
     */
    public function __construct(string $ocrWorkDirectory, OcrDocumentFactoryInterface $ocrDocumentFactory)
    {
        file_put_contents('debug.log', $ocrWorkDirectory, FILE_APPEND);
        $this->setWorkDir($ocrWorkDirectory);
        $this->ocrDocumentFactory = $ocrDocumentFactory;
    }


    /**
     * @param string $DocumentData
     * @param string $mimeType
     * @return array
     * @throws HocrCreationFailedException
     */
    public function convertToHocr(string $DocumentData, string $mimeType): array
    {
        $file_types = ['application/pdf' => 'pdf', 'image/tiff' => 'tiff', 'image/jpeg' => 'jpg', 'image/png' => 'png'];
        $files = [];
        $mime_type = strtolower($mimeType);
        try {
            $image_file = tempnam($this->getWorkDir(), $file_types[$mime_type] . '_');
            file_put_contents($image_file, $DocumentData);
            $files[] = $image_file;
            $images = [];
            if ($mime_type == 'application/pdf') {
                $im = new Imagick();
                $im->setResolution(300, 300);
                $im->readImage($image_file);
                $im->setImageFormat('tiff');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_DEACTIVATE);
                $im->setCompression(Imagick::COMPRESSION_GROUP4);
                $mime_type = 'image/tiff';
                $image_file .= '.tiff';
                $im->writeImages($image_file, true);
                $files[] = $image_file;
                foreach ($im as $i => $image) {
                    $image->setImageFormat('png');
                    $images[] = ['image/png' => $image->getImageBlob()];
                }

            } else {
                $images[] = [$mime_type => $DocumentData];
            }
            $tesseract = new TesseractOCR($image_file);
            $tesseract->config("tessedit_create_hocr", 1);
            $hocr_file = tempnam($this->getWorkDir(), 'hocr_');
            $tesseract->lang("dan", "deu", "eng", "nor");
            $tesseract->configFile('hocr');
            $tesseract->setOutputFile($hocr_file);
            $hocr = $tesseract->run();
            // $hocr = file_get_contents($hocr_file);
            $files[] = $hocr_file;
            return [$hocr, $images];
        } catch (Exception $e) {
            file_put_contents('debug.log', $e->getMessage(), FILE_APPEND);
            die();
            throw new HocrCreationFailedException($e->getMessage());
        } finally {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }

    /**
     * @param string $hocr
     * @param array $images
     * @return OcrDocument
     */
    public function convertHocrToOcrDocument(string $hocr, array $images) : OcrDocument
    {
        return $this->ocrDocumentFactory->make($hocr, $images);
    }

    /**
     * @return string
     */
    public function getWorkDir(): string
    {
        return $this->workDir;
    }

    /**
     * @param string $workDir
     */
    public function setWorkDir(string $workDir): void
    {
        $this->workDir = $workDir;
    }

} // class OcrDocument
