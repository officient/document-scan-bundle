<?php

namespace Officient\DocumentScan\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * This class describes the configuration for this bundle
 *
 * @package Officient\DocumentScan\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('document_scan');

        $treeBuilder->getRootNode()
            ->children()
            ->scalarNode('ocr_work_directory')->defaultValue('/tmp')->end();

        return $treeBuilder;
    }
}