<?php

namespace Officient\DocumentScan\DependencyInjection;

use Exception;
use Officient\DocumentScan\Exception\DocumentScanException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class DocumentScanExtension
 *
 * This class handles loading services and configurations
 *
 * @package Officient\DocumentScan\DependencyInjection
 */
class DocumentScanExtension extends Extension
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');

        $serviceDef = $container->getDefinition('Officient\DocumentScan\Service\OcrService');
        $serviceDef->replaceArgument(0, $config['ocr_work_directory']);
    }
}